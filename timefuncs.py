import datetime as dt
from os import path, makedirs, listdir, remove, chdir
from os.path import abspath
from errno import EEXIST # needed for exception handling
from json import load # read json data
from sys import stderr
from sys import exit
from base64 import b64decode

# loads config file for pointcarbon client
configfile = abspath('wapio_config.txt')
try:
    configs = load(open(configfile, 'r'))
except IOError:
    stderr.write("File " + configfile + " can't be opened")
    exit(1)

# returns a dict with user credentials
def get_credentials():
    with open(abspath(configs["credentials_file"]), 'r') as f:
        lines = f.read().splitlines()
    dict = {"user": lines[0], "pass": b64decode(lines[1]).decode()}
    return dict

# returns a value from config dict.
# INPUT: string key - a dict key
def get_config(key):
    return configs[key]

# reads input file and returns an array of dicts, each one describing a curve
def read_inputf():
    inputfile = abspath(get_config("curves_input_file"))
    try:
        curves = load(open(inputfile))
        writeToLog("Curve input file opened and parsed.")
        return curves
    except IOError:
        writeToLog("Cannot open input file " + inputfile)
        exit(1)

# returns current time and date for logging
def dateStringTime():
    return dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

# Writes a message string to a log file, location of which is specified in config file
# INPUT: string message
def writeToLog(message):
    filename = configs["log_file"]
    with open(filename, 'a') as log:
        log.write(dateStringTime() + ": " + message + "\n")

# check if output directory exists. Create if not
def check_output_dir():
    out_dir = get_config("output_dir")
    if not path.exists(path.dirname(out_dir)):
        try:
            makedirs(path.dirname(out_dir))
        except OSError as exc: # Guard against race condition
            if exc.errno != EEXIST:
                raise

# Clear previous output files from directory. ONLY those from pcarbon
def clear_output_files():
    charts = read_inputf()
    chdir(get_config("output_dir"))
    for c in charts:
        files = listdir()
        for f in files:
            if c['key'] in f:
                remove(f)
    chdir('..')
