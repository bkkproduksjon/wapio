import urllib3
import requests
import csv
import timefuncs as t
from timefuncs import get_config
urllib3.disable_warnings()

t.writeToLog("---- Started PC client ----")
t.check_output_dir()
t.clear_output_files()
cred = t.get_credentials()
user = cred["user"]
passw = cred["pass"]

# getting list of files if get_file_list is 1 in config
if int(get_config("get_list_file")):
    r = requests.get('https://download.wattsight.com/list-files?response-as=csv', auth=(user, passw), verify=False)
    if r.status_code == 200:
        decoded = r.content.decode('utf-8')
        cr = csv.reader(decoded.splitlines(), delimiter=';')
        rows = list(cr)
        with open(get_config("output_dir") + 'file_list.csv', 'w', newline='') as fil:
            cw = csv.writer(fil, delimiter=";", quotechar='\"', quoting=csv.QUOTE_NONNUMERIC, dialect='excel')
            cw.writerows(rows)
        t.writeToLog("Downloaded file_list.csv")
    else:
        t.writeToLog("Failed to download file_list.csv")

# getting files
input = t.read_inputf()
for inpt in input:
    r = requests.get('https://download.wattsight.com/download-file?file=' + inpt["key"] + '&area=' + inpt["area"], auth=(user, passw), verify=False)
    if r.status_code == 200:
        decoded = r.content.decode('utf-8')
        cr = csv.reader(decoded.splitlines(), delimiter=';')
        rows = list(cr)
        with open(get_config("output_dir") + inpt["key"] + '_' + inpt["area"] + '.csv', 'w', newline='') as fil:
            cw = csv.writer(fil, delimiter=";", quotechar='\"', quoting=csv.QUOTE_NONNUMERIC, dialect='excel')
            cw.writerows(rows)
        t.writeToLog("Downloaded " + inpt["key"] + "_" + inpt["area"])
    else:
        t.writeToLog("Failed to download " + inpt["key"] + "_" + inpt["area"])

t.writeToLog("---- Wapio stopped ----")
